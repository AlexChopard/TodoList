package alex.chopard.mytodolist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by AlexChop on 15/01/2018.
 */

public class TodoActivity extends Activity {

    ListView lv_lestextes;
    Button btn_ajouter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.task_layout);

        lv_lestextes = findViewById(R.id.lv_lestextes);
        btn_ajouter = findViewById(R.id.btn_ajouter);

        majListView();

        btn_ajouter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TodoActivity.this, AjouterTodoActivity.class);
                startActivity(intent);
            }
        });

        /*lv_lestextes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView t = (TextView) view;
                t.setTextColor(getResources().getColor(R.color.todo));
            }
        });*/

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        majListView();
    }

    public void majListView(){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, Singleton.getInstance().getMesTextes());
        lv_lestextes.setAdapter(adapter);

        TextView t = findViewById((int) lv_lestextes.getItemIdAtPosition(0));
        //t.setTextColor(getResources().getColor(R.color.todo));


    }
}
