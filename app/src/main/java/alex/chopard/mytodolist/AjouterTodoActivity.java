package alex.chopard.mytodolist;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by AlexChop on 15/01/2018.
 */

public class AjouterTodoActivity extends Activity {


    EditText et_text;
    Button btn_valider, btn_vider;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ajoutertodo_activity);

        et_text =  findViewById(R.id.et_edit);
        btn_valider = findViewById(R.id.btn_valider);
        btn_vider = findViewById(R.id.btn_vider);

        btn_valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = et_text.getText().toString();
                Singleton.getInstance().addMesTextes(text);
                et_text.setText("");
                finish();
            }
        });

        btn_vider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Singleton.getInstance().setMesTextes(new ArrayList<String>());
                et_text.setText("");
                finish();
            }
        });


    }
}
