package alex.chopard.mytodolist;

import java.util.ArrayList;

/**
 * Created by AlexChop on 15/01/2018.
 */

public final class Singleton {

    private static volatile Singleton instance = null;

    private ArrayList<String> mesTextes;

    private Singleton(){
        super();
        mesTextes = new ArrayList<>();
    }

    public final static Singleton getInstance(){
        if(Singleton.instance == null){
            synchronized (Singleton.class){
                if(Singleton.instance == null){
                    Singleton.instance = new Singleton();
                }
            }
        }

        return Singleton.instance;
    }


    public ArrayList<String> getMesTextes(){
        return this.mesTextes;
    }

    public boolean setMesTextes(ArrayList<String> textes){
        mesTextes = textes;
        return true;
    }

    public boolean addMesTextes(String texte){
        mesTextes.add(texte);
        return true;
    }

}
